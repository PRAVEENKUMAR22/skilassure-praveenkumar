package beginner;

public class Lab_8_lowercase_to_uppercase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String s1="heLLo WoRLD";
		StringBuffer s2=new StringBuffer(s1);
		
		for (int i=0; i<s1.length(); i++)
		{
			if(Character.isLowerCase(s1.charAt(i))) 
			{
				s2.setCharAt(i,Character.toUpperCase(s1.charAt(i)));
			}
			
			else if(Character.isUpperCase(s1.charAt(i))) 
			{
				s2.setCharAt(i, Character.toLowerCase(s1.charAt(i)));
			}
			
		}
		System.out.println("String after case conversion:  " + s2);
	}

}
