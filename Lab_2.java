package beginner;

import java.util.Scanner;

//      swapping two numbers_numbers

public class Lab_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 
		   //1.intiallization
		   int a, b, c;  
		   
		   //creating scanner object;
	       Scanner sc = new Scanner(System.in);  
	       System.out.println("Enter the value of a and b");  
	       a = sc.nextInt();  
	       b = sc.nextInt(); 
	       
	       //display the output;
	       System.out.println("before swapping numbers: "+a +"  "+ b); 
	       c = a;  
	       a = b;  
	       b = c;  
	       System.out.println("After swapping: "+a +"   " + b);  
	       
	    }    
}
