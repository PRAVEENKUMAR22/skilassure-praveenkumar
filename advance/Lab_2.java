package advance;

public class Lab_2 {

	public static int linearSearch(int array[], int x) {
		  int n = array.length;

		  // Going through array 
		  for (int i = 0; i < n; i++) {
		    if (array[i] == x)
		    return i;
		  }
		  return -1;
		  }

	      // main method:
		  public static void main(String args[]) {
		  int array[] = { 2, 4, 0, 1, 9 };
		  int x = 2;

		  int result = linearSearch(array, x);

		  if (result == -1)
		    System.out.print("Element not found");
		  else
		    System.out.print("Element found at index: " + result);
		  }

}
