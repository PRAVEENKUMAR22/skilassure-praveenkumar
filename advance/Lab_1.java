package advance;

public class Lab_1 {
	
	 public static void main(String[] args)
	 {
		int row=4,column=3;
		int matrix [][] = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		
		//display matrix
		display(matrix);
		
		//transpose the matrix
		int [][] transpose = new int[column][row];
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<column;j++)
			{
				transpose [j][i] = matrix [i][j];
			}
		}
		
		///display the transpose
		display(transpose);
		
	 }
	 public static void display(int[][] matrix)
	 {
		 System.out.println("The matrix is");
		 
		 for(int[] row:matrix)
		 {
			 for(int column:row)
			 {
				 System.out.print(column+" ");
			 }
			 System.out.println();
		 }
	 }

}
