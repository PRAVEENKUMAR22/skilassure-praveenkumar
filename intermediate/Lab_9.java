package intermediate;

public class Lab_9 {

	
	public static void main(String[] args) 
	{
		//declaration
		int a,b,c,temp,answer;
		
		//initialization 
		a = 25;
		b = 55;
		c = 19;
		
		//ternary operator
		temp = a < b ? a:b;
		answer = c < temp ? c:temp;
		
		System.out.println("the smallest number is "+answer);

      }

  }